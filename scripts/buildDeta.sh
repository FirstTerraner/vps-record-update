#!/bin/bash

# echo $PWD
###opts
# set -e          #exit on error
set -o pipefail #fail on error in pipechain
set -x          #debug
time(
	npx tsc
	cp -r dist/* ./
	deta deploy
	rm -rf models index.d.ts index.d.ts.map dist core index.js* &
)
