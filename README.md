


## Installing from source

### Linux, MacOS, Windows, *BSD, Solaris, WSL, Android, Raspbian

```bash
git clone https://gitlab.com/FirstTerraner/vps-record-update.git
cd vps-record-update
npm i
scripts/ssl.sh
npm start
```

## Deploy to [Deta.sh](https://deta.sh)

Install the Deta CLI [docs.deta.sh/docs/cli/install](https://docs.deta.sh/docs/cli/install)

### Linux, MacOS, Windows, *BSD, Solaris, WSL, Android, Raspbian

```bash
git clone https://gitlab.com/FirstTerraner/vps-record-update.git
cd vps-record-update
npm i
npx tsc
cp -r dist/* ./
deta new
deta update -e .env
deta cron set "15 minutes"
npm run bb

```

## update [Deta.sh](https://deta.sh) Micro

Install the Deta CLI [docs.deta.sh/docs/cli/install](https://docs.deta.sh/docs/cli/install)

### Linux, MacOS, Windows, *BSD, Solaris, WSL, Android, Raspbian

```bash

npm run bb

```


That's it!
