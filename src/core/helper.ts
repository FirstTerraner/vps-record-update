/* eslint-disable @typescript-eslint/ban-types */
export function tryParseJson<T, TDefault>(json: string | null, defaulValue:TDefault) {
	if (!json || json?.length < 2) { return defaulValue }
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const thing: T = JSON.parse(json)
		return thing
	} catch (e) { console.log(e) }
	return defaulValue
}
export function cTo<T>(json?: string | null) {
	if (!json || json?.length < 2) { return null }
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const thing: T = JSON.parse(json)
		return thing
	} catch (e) { console.log(e) }
	return null
}
export function toJson<T>(thing: T) { try { return JSON.stringify(thing) } catch (e) { return null } }



export function getRandomInt(min: number, max: number) { return Math.floor(Math.random() * max) + min }
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isArr = (v: unknown): v is any[] => !!((v && Array.isArray(v)) || (v && typeof v === 'object' && v.constructor === Array))
export const isBool = (v: unknown): v is boolean => typeof v === 'boolean'
// eslint-disable-next-line @typescript-eslint/no-explicit-any
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
export const isErr = (v: any): v is Error => !!(v instanceof Error || (v && v.__proto__ && v.__proto__.name === 'Error'))
// tslint:disable-next-line: ban-types
export const isFunc = (v: unknown): v is Function => typeof v === 'function'
export const isNull = (v: unknown): v is null => v === null
export const isNum = (v: unknown): v is number => typeof v === 'number' && !isNaN(v) && isFinite(v)
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
export const isObj = (v: unknown): v is object => !!(v && typeof v === 'object' && v.constructor === Object)
export const isStr = (v: unknown): v is string => typeof v === 'string' || v instanceof String
export const isUndef = (v: unknown): v is undefined => typeof v === 'undefined'









export function msToHMS(ms: number) {
	// 1- Convert to seconds:
	let seconds = ms / 1000
	// 2- Extract hours:
	const hours = Math.floor(seconds / 3600) // 3,600 seconds in 1 hour
	seconds = seconds % 3600 // seconds remaining after extracting hours
	// 3- Extract minutes:
	const minutes = Math.floor(seconds / 60) // 60 seconds in 1 minute
	// 4- Keep only seconds not extracted to minutes:
	seconds = Math.round(seconds % 60)
	return `${hours}:${minutes}:${seconds.toFixed(3)}`
}
export const getUnixTimestamp = () => Math.floor(Date.now() / 1000)
export type StopTime = { endTime: number, hrEnd: [number, number]; hrStart: [number, number], startTime: number; }

export function getTime(): StopTime { return { startTime: new Date().valueOf(), hrStart: process.hrtime(), endTime: 0, hrEnd: [0, 0] } }
export function printTime(time: StopTime, full = true, msg = '') {
	time.endTime = new Date().valueOf() - time.startTime.valueOf()
	time.hrEnd = process.hrtime(time.hrStart)
	// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
	console.info(`[${new Date().toLocaleTimeString()}][${callerInfo()?.name}]`, msg, `Execution time: ${time.endTime} ms`)
	if (!full) { return }
	// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
	console.info(`[${new Date().toLocaleTimeString()}][${callerInfo()?.name}]`, `Execution time (hr): ${time.hrEnd[0]} s, ${time.hrEnd[1] / 1000000} ms`)
	// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
	console.info(`[${new Date().toLocaleTimeString()}][${callerInfo()?.name}]`, 'Execution time: ', msToHMS(time.endTime))
}
export function callerInfo(skipOf = 3) {
	skipOf = skipOf || 3
	let eStack
	try { eStack = new Error() } catch (err) { console.info('kaka', undefined) }
	// console.log(eStack?.stack?.split('at '), '\n', eStack?.stack?.split('at ')[skipOf])
	let tmpv = eStack?.stack?.split('at ')[skipOf]?.split(')\n')[0]?.split(' (')
	if (!tmpv || !tmpv[1] || !tmpv[0]) { tmpv = eStack?.stack?.split('at ')[skipOf + 1]?.split(')\n')[0]?.split(' (') }
	/* const error = new Error('')
	// console.log(error.stack, '\n', '\n')
	if (error.stack) {
		const cla = error.stack.split('\n')
		let idx = 1

		console.log(idx, '----------------', cla[idx])
		while (idx < cla.length && cla[idx].includes('callerInfo')) { idx++ }
		if (idx < cla.length) {
			a = cla[idx].slice(cla[idx].indexOf('at ') + 3, cla[idx].length)
		}
	}
	if (!tmpv || !tmpv[1] || tmpv[1] === undefined) { console.log(eStack?.stack) }*/
	if (tmpv) { return { name: tmpv[0].replace('Object.exports.', '').replace('Object.', ''), path: tmpv[1] } }
	return null
}
