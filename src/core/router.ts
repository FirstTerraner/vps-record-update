import express from 'express'
import { doUpdate,  milSectoWait } from '.'
import { redisClient } from './redis'

// eslint-disable-next-line new-cap
export const router = express.Router()

// const badResp = { response: 'data not available, check url params' }

/* GET */
// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
router.get('/', (_req, res) => { res.sendFile(`${process.env.FRONTEND}/index.html`) })

// ping server
router.get('/api/ping/local', (_req, res) => {
	console.log('ping')
	res.send({ response: 'pong' })
})

// get last update start old bad
/* router.get('/api/time_bad', (_req, res) => {
	const milSecSinceLastUpdate = Date.now() - lastUpdateTime
	const milSecToWaitLeft = milSectoWait - milSecSinceLastUpdate
	console.log('time', { lastUpdateStart: lastUpdateTime }, { lastUpdateStartDate: new Date(lastUpdateTime) })
	console.log(1000 * 60 * 20)
	res.send({
		milSectoWait,
		lastUpdateTime,
		milSecSinceLastUpdate,
		milSecToWaitLeft
	})
}) */
// get last update start
// eslint-disable-next-line @typescript-eslint/no-misused-promises
router.get('/api/time', async (_req, res) => {
	const client = redisClient()
	if (!client) { return }
	const t = await client.get('record-updated')
	if (t && +t > 0) {
		const lastUpdateTime = +t * 1000
		console.log((new Date(lastUpdateTime)).toLocaleString())
		const milSecSinceLastUpdate = Date.now() - lastUpdateTime
		const milSecToWaitLeft = milSectoWait - milSecSinceLastUpdate
		console.log('time', { lastUpdateStart: lastUpdateTime }, { lastUpdateStartDate: new Date(lastUpdateTime) })
		console.log(1000 * 60 * 15)
		res.send({
			milSectoWait,
			lastUpdateTime,
			milSecSinceLastUpdate,
			milSecToWaitLeft
		})
		return
	}
	res.send({msg:'bad redis client or update time'})
})

// doUpdate
// eslint-disable-next-line @typescript-eslint/no-misused-promises
router.get('/api/doupdate', async (_req, res) => {
	try {
		await doUpdate()
		res.send({ status: 'done' })
	} catch (err) { res.send({ status: err })}
})