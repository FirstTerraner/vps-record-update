import { WrappedNodeRedisClient } from 'handy-redis'
import { getClubStats, TPlatformType } from 'proclubs-api'
import { IRecClub } from '../models/vps'
import { getUnixTimestamp, isArr, tryParseJson } from './helper'
import { redisClient } from './redis'

export const updateRecord = async () => {

	console.log('start update record clubs...')

	const client = redisClient()
	if (!client) { return }

	const updatedArgsArr: [key: string, value: string][] = []

	// get all current records
	const recsStr = await client.mget('ps4-record', 'ps5-record', 'xboxone-record', 'xbox-series-xs-record', 'pc-record')

	// parse all current records
	const ps4RedisRecs: IRecClub[] = tryParseJson<IRecClub[], IRecClub[]>(recsStr[0], [])
	const ps5RedisRecs: IRecClub[] = tryParseJson<IRecClub[], IRecClub[]>(recsStr[1], [])
	const xb1RedisRecs: IRecClub[] = tryParseJson<IRecClub[], IRecClub[]>(recsStr[2], [])
	const xbsxsRedisRecs: IRecClub[] = tryParseJson<IRecClub[], IRecClub[]>(recsStr[3], [])
	const pcRedisRecs: IRecClub[] = tryParseJson<IRecClub[], IRecClub[]>(recsStr[4], [])

	// updated records after fetch
	const ps4UpdatedRecs: IRecClub[] = []
	const ps5UpdatedRecs: IRecClub[] = []
	const xb1UpdatedRecs: IRecClub[] = []
	const xbsxsUpdatedRecs: IRecClub[] = []
	const pcUpdatedRecs: IRecClub[] = []

	// fetching platform independently to know in which the clubs are registered


	// fetch ps4 clubs
	if (ps4RedisRecs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(ps4RedisRecs, ps4UpdatedRecs, 'ps4')
		// write new recs after loop fetches
		if (isArr(ps4UpdatedRecs) && ps4UpdatedRecs?.length) { updatedArgsArr.push(['ps4-record', JSON.stringify(ps4UpdatedRecs)]) }
	}

	// fetch ps5 clubs
	if (ps5RedisRecs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(ps5RedisRecs, ps5UpdatedRecs, 'ps5')
		// write new recs after loop fetches
		updatedArgsArr.push(['ps5-record', JSON.stringify(ps5UpdatedRecs)])
	}

	// fetch xb1 clubs
	if (xb1RedisRecs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(xb1RedisRecs, xb1UpdatedRecs, 'xboxone')
		// write new recs after loop fetches
		updatedArgsArr.push(['xboxone-record', JSON.stringify(xb1UpdatedRecs)])
	}

	// fetch xbsxs clubs
	if (xbsxsRedisRecs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(xbsxsRedisRecs, xbsxsUpdatedRecs, 'xbox-series-xs')
		// write new recs after loop fetches
		updatedArgsArr.push(['xbox-series-xs-record', JSON.stringify(xbsxsUpdatedRecs)])
	}

	// fetch pc clubs
	if (pcRedisRecs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(pcRedisRecs, pcUpdatedRecs, 'pc')
		// write new recs after loop fetches
		updatedArgsArr.push(['pc-record', JSON.stringify(pcUpdatedRecs)])
	}

	/* console.log('ps4UpdatedRecs: ', ps4UpdatedRecs)
	console.log('ps5UpdatedRecs: ', ps5UpdatedRecs)
	console.log('xb1UpdatedRecs: ', xb1UpdatedRecs)
	console.log('xbsxsUpdatedRecs: ', xbsxsUpdatedRecs)
	console.log('pcUpdatedRecs: ', pcUpdatedRecs) */

	// write updated clubs
	await client.mset(...updatedArgsArr)

	const allClubs = updatedArgsArr.flatMap(x => tryParseJson<IRecClub[], IRecClub[]>(x[1], []))

	await setHighestRec(allClubs, client)
	await client.set('record-updated', getUnixTimestamp().toString())
	await client.quit()
}

const fetchClubs = async (redisRecords: IRecClub[], platUpdatedRecs: IRecClub[], plat: TPlatformType) => {
	console.log(`start fetching ${plat} clubs now...`)
	if (redisRecords.length > 0) {
		// console.log(`${plat} record clubs: `, platformRecs)
		try {
			for (const club of redisRecords) {
				// skip if club has losses
				if (club.hasLost) {
					console.log(`No need to fetch loosing club: ${club.name}, write the already available club...`)
					platUpdatedRecs.push({
						...club,
						hasLost: true,
						squadList: [],
						addedAt: club.addedAt,
						name: club.name,
						plat
					})
					continue
				}
				// eslint-disable-next-line no-await-in-loop
				const fetchedClub = await getClubStats(plat, +club.clubId)

				// only update if club is not deleted and still has no losses
				if (fetchedClub?.clubId && fetchedClub?.losses === '0') {

					platUpdatedRecs.push({
						...fetchedClub,
						clubInfo: club.clubInfo,
						hasLost: false,
						squadList: [],
						addedAt: club.addedAt,
						name: club.name,
						plat
					})
				} else {
					console.log(`club: ${club.name} has been deleted, write the already available club...`)
					platUpdatedRecs.push({
						...club,
						hasLost: true,
						squadList: [],
						addedAt: club.addedAt,
						name: club.name,
						plat
					})
				}
			}
		} catch (e) {
			console.log(`error while fetching ${plat} clubs`)
		}
	}
}

// get highest record cross platform
const setHighestRec = async (
	allRecClubs: IRecClub[],
	client: WrappedNodeRedisClient
) => {

	console.log('set highest record')

	// sort array from highest record to lowest
	allRecClubs.sort((a, b) => {
		const pointsA = +a.wins * 3 + +a.ties
		const pointsB = +b.wins * 3 + +b.ties
		const goalDiffA = +a.alltimeGoals - +a.alltimeGoalsAgainst
		const goalDiffB = +b.alltimeGoals - +b.alltimeGoalsAgainst
		// -1 more points
		if (pointsA > pointsB) { return -1 }
		// -1 same points, more goals
		if (pointsA === pointsB && goalDiffA > goalDiffB) { return -1 }
		// 0 same points, same goals, less goalsAgainst
		if (pointsA === pointsB && goalDiffA === goalDiffB) { return 0 }
		// else
		return 1
	})

	// TODO it is possible that multiple clubs have the same points so we return them all
	// otherwise we return the highest one
	const highest: IRecClub = allRecClubs[0]

	console.log('highest: ', highest.name)
	console.log('record: ', highest.wins)
	await client.set('highest-record', JSON.stringify(highest))
	console.log('done')

}

// const resetRedis = async () => {
// 	const client = redisClient()
// 	if (!client) { return }

// 	await client.flushall()
// 	await Promise.all([
// 		client.set('AT', '251'),
// 		client.set('ps4-record', '[]'),
// 		client.set('ps5-record', '[]'),
// 		client.set('xboxone-record', '[]'),
// 		client.set('xbox-series-xs-record', '[]'),
// 		client.set('pc-record', '[]')
// 	])

// 	await client.quit()
// }