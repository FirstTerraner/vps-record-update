/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable new-cap */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { json, urlencoded } from 'body-parser'
import cors from 'cors'
import { App } from 'deta'
import { config } from 'dotenv'
import express from 'express'
// import { openSocket } from './externalApis/exchanges/binance/websockets'
import { readFileSync } from 'fs'
import { createServer } from 'https'
import { join } from 'path'
import { getTime, printTime } from './helper'
import { router } from './router'
import { updateRecord } from './update'

export const app = App(express())
// dotenv
config()

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
app.lib.cron(async (event: any) => {
	// const client = redisClient()
	const r = await doUpdate()
	console.log({ event }, { r })
	return `it is ${(new Date()).toTimeString()}`
})
export let lastUpdateStartTime: number
export const milSectoWait = 1000 * 60 * 15
process.on('uncaughtException', err => {
	console.error(err, 'uncaughtException')
	process.exit(1)
})

process.on('unhandledRejection', err => {
	console.error(err, 'unhandledRejection')
	process.exit(1)
})


// setup
const host = process.env.SERVER_HOST
const port = process.env.SERVER_PORT
const appMode = process.env.NODE_ENV

app.use(urlencoded({ extended: false }))
app.use(json())
app.use(cors())
app.use(router)
// app use frontend
if (process.env.FRONTEND && process.env.FRONTEND !== '') {
	app.use(express.static(join(process.env.FRONTEND)))
}

export const doUpdate = async () => {
	const time = getTime()
	lastUpdateStartTime = Date.now()
	await updateRecord()
	printTime(time, true, 'updates done')
}

// const detaPath = process?.env?.DETA_PATH
// const detaRuntime = process?.env?.DETA_RUNTIME
// const detaProjectKey = process?.env?.DETA_PROJECT_KEY
const isRunningOnDeta = () => !!process?.env?.DETA_RUNTIME

const start = async () => {
	if (!isRunningOnDeta()) {
		// start updates now !
		await doUpdate()
		// 20 mins interval for update record clubs
		setInterval(() => { void doUpdate() }, milSectoWait)
		// ssl
		const sslKey = readFileSync('ssl/key.pem', 'utf8')
		const sslCert = readFileSync('ssl/cert.pem', 'utf8')
		// create server
		const httpsServer = createServer({ key: sslKey, cert: sslCert }, app)
		/* httpsServer.on('upgrade', function upgrade(request, socket, head) {
			const webSocket = openSocket(request.url || '')
			webSocket.handleUpgrade(request, socket, head, function done(ws) {
				webSocket.emit('connection', ws, request)
			})
		}) */
		// start server
		httpsServer.listen(port, () => {
			const fe = process.env.FRONTEND !== '' ? process.env.FRONTEND : 'Not provided. Setup -.env file & restart.'
			console.log(
				// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
				`\nAPI documentation: ${process.env.API_DOCS}\n\n${host}:${port} | ${appMode} mode | Frontend: ${fe}\n`
			)
		})
	}
}

void start()
