import { TPlatformType } from 'proclubs-api'

export interface IRecPlayer {
	name: string
	playedMatches?: number // not included yet
	pos: string // position
}

export interface IRecClub extends IClubSearch {
	addedAt: string
	/* while updating record clubs entries & seeing a loss or club deleted,
	update hasLost value so we know there is no need to fetch the club next time */
	hasLost: boolean
	plat: TPlatformType
	squadList: IRecPlayer[]
}

export interface IClubInfo {
	clubId: number
	customKit: ICustomKit
	name: string
	regionId: number
	teamId: number
}

export interface ICustomKit {
	crestAssetId: string
	crestColor: string
	customAwayKitId: string
	customKeeperKitId?: string
	customKitId: string
	dCustomKit: string
	isCustomTeam: string
	kitAColor1: string
	kitAColor2: string
	kitAColor3: string
	kitColor1: string
	kitColor2: string
	kitColor3: string
	kitId: string
	stadName: string
}

export interface IClubSearch {
	alltimeGoals: string
	alltimeGoalsAgainst: string
	bestDivision: number
	bestPoints: string
	clubId: string
	clubInfo: IClubInfo
	cupRankingPoints: string
	cupsElim0: string
	cupsElim0R1: string
	cupsElim0R2: string
	cupsElim0R3: string
	cupsElim0R4: string
	cupsElim1: string
	cupsElim1R1: string
	cupsElim1R2: string
	cupsElim1R3: string
	cupsElim1R4: string
	cupsElim2: string
	cupsElim2R1: string
	cupsElim2R2: string
	cupsElim2R3: string
	cupsElim2R4: string
	cupsElim3: string
	cupsElim3R1: string
	cupsElim3R2: string
	cupsElim3R3: string
	cupsElim3R4: string
	cupsElim4: string
	cupsElim4R1: string
	cupsElim4R2: string
	cupsElim4R3: string
	cupsElim4R4: string
	cupsElim5: string
	cupsElim5R1: string
	cupsElim5R2: string
	cupsElim5R3: string
	cupsElim5R4: string
	cupsElim6: string
	cupsElim6R1: string
	cupsElim6R2: string
	cupsElim6R3: string
	cupsElim6R4: string
	cupsWon0: string
	cupsWon1: string
	cupsWon2: string
	cupsWon3: string
	cupsWon4: string
	cupsWon5: string
	cupsWon6: string
	currentDivision: number
	curSeasonMov: string
	divsWon1: number
	divsWon2: number
	divsWon3: number
	divsWon4: number
	gamesPlayed: string
	goals: string
	goalsAgainst: string
	holds: string
	lastMatch0: string
	lastMatch1: string
	lastMatch2: string
	lastMatch3: string
	lastMatch4: string
	lastMatch5: string
	lastMatch6: string
	lastMatch7: string
	lastMatch8: string
	lastMatch9: string
	lastOpponent0: string
	lastOpponent1: string
	lastOpponent2: string
	lastOpponent3: string
	lastOpponent4: string
	lastOpponent5: string
	lastOpponent6: string
	lastOpponent7: string
	lastOpponent8: string
	lastOpponent9: string
	leaguesWon: string
	losses: string
	maxDivision: string
	name: string
	overallRankingPoints: string
	points: string
	prevDivision: string
	prevPoints: string
	prevProjectedPts: string
	prevSeasonLosses: string
	prevSeasonTies: string
	prevSeasonWins: string
	projectedPoints: number
	promotions: string
	rankingPoints: string
	recentResults: RecentResult[]
	relegations: string
	seasonLosses: string
	seasons: number
	seasonTies: string
	seasonWins: string
	skill: string
	starLevel: string
	ties: string
	titlesWon: string
	totalCupsWon: number
	totalGames: number
	wins: string
}

declare enum RecentResult {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	Empty = '',
	// eslint-disable-next-line @typescript-eslint/naming-convention
	Losses = 'losses',
	// eslint-disable-next-line @typescript-eslint/naming-convention
	Ties = 'ties',
	// eslint-disable-next-line @typescript-eslint/naming-convention
	WINS = 'wins'
}